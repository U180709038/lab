package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

	public Cube() {
		super(1.0);
	}
	public Cube(double side) {
		super(side);
	}
	public double area() {
		return super.area()*6;		
	}
	public double volume() {
		return super.area()*this.getSide();
	}
	public String toString() {
		return "Cube side: "+this.getSide();
	}

}