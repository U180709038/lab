package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
	private double height;
	public Cylinder() {
		super();
		this.height=1.0;
	}
	public Cylinder(double radius,double height) {
		this.setRadius(radius);
		this.height=height;		
	}
	public double getHeight() {
		return this.height;
	}
	public void setHeight(double height) {
		this.height=height;	
	}
	public double area() {
		return (super.area()*2)+(Math.PI*this.getRadius()*this.height);
	}
	public double volume() {
		return super.area()*this.height;
	}
	public String toString() {
		return "Cylinder height: "+this.height+" Cylinder radius: "+getRadius();	
	}
}