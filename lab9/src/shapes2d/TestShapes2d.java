package shapes2d;

public class TestShapes2d {
	public static void main(String[] args) {
		Circle a=new Circle();
		Circle d=new Circle(4);
		Square e=new Square();
		Square f=new Square(10);
		System.out.println("area of the first circle: "+a.area());
		System.out.println("area of the second circle: "+d.area());
		System.out.println("area of the first square: "+e.area());
		System.out.println("area of the second square: "+f.area());
		System.out.println("first circle "+a.toString());
		System.out.println("second circle "+d.toString());
		System.out.println("first square "+e.toString());
		System.out.println("second square "+f.toString());
		
	
	}
	

}