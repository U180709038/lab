

public class Circle {
	int radius;
	Point center;
	
	public Circle(int radius, Point center) {
		this.radius = radius;
		this.center = center;		
	}
	
	public double area() {
		return (radius * radius * Math.PI);		
	}
	
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
	
	
	
}
