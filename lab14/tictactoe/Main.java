package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InvalidMoveException  {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();
		
		System.out.println(board);
		while (!board.isEnded()) {
			
			int player = board.getCurrentPlayer();
			
			
			
			boolean flag= false;
			int row = 0;
			do {
				
				try {
					System.out.print("Player "+ player + " enter row number:");
					row = reader.nextInt();
					flag=false;
				}
				
				catch(InputMismatchException e) {
					System.out.println("Please give an integer value");
					flag = true;
					reader.nextLine();
				}
			}
			while(flag);
			
			
			
			
			
			boolean flag2= false;
			int col = 0;
			do {
				
				try {
					System.out.print("Player "+ player + " enter column number:");
					col = reader.nextInt();
					flag2=false;
				}
				
				catch(InputMismatchException e) {
					System.out.println("Please give an integer value");
					flag2 = true;
					reader.nextLine();
				}
			}
			while(flag2);
			
			
			
			board.move(row, col);
			System.out.println(board);
		}
		
		
		reader.close();
	}



}
