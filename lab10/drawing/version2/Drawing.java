package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;


public class Drawing {
	
    ArrayList<Object> shapes=new ArrayList<Object>();
	
	public double calculateTotalArea(){
		double totalArea = 0;
        for (Object shape:shapes){
            if(shape instanceof Circle){
                Circle circle=(Circle)shape;
                totalArea+=circle.area();
            }else if (shape instanceof Rectangle){
                Rectangle rectangle=(Rectangle)shape;
                totalArea+=rectangle.area();
            }else if(shape instanceof Square){
                Square square=(Square)shape;
                totalArea+=square.area();
            }
                
        }

		return totalArea;
	}
	
	public void addShape(Object o) {
		shapes.add(o);
	}
	

}